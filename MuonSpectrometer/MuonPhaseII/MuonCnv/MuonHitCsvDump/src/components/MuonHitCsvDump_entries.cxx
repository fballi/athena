
/*
   Copyright (C) 2002-2025 CERN for the benefit of the ATLAS collaboration
*/
#include "../SpacePointCsvDumperAlg.h"
#include "../TruthSegmentCsvDumperAlg.h"
DECLARE_COMPONENT(MuonR4::SpacePointCsvDumperAlg)
DECLARE_COMPONENT(MuonR4::TruthSegmentCsvDumperAlg)